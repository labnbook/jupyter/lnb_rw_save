import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { INotebookTracker } from '@jupyterlab/notebook';

import { ISettingRegistry } from '@jupyterlab/settingregistry';

/**
 * Initialization data for the lnb_rw_save extension.
 */
const plugin: JupyterFrontEndPlugin<void> = {
  id: 'lnb_rw_save:plugin',
  autoStart: true,
  optional: [ISettingRegistry],
  requires: [INotebookTracker],
  activate: (app: JupyterFrontEnd, notebookTracker:INotebookTracker, settingRegistry: ISettingRegistry | null) => {

    const id_ld: any = window.frameElement.id.split('ld_code_iframe_').pop();

    if (settingRegistry) {
      settingRegistry
        .load(plugin.id)
        .then(settings => {
          console.log('lnb_rw_save: settings loaded [id_labdoc='+id_ld+'].', settings.composite);
        })
        .catch(reason => {
          console.error('lnb_rw_save: failed to load settings [id_labdoc='+id_ld+'].', reason);
        });
    }

    notebookTracker.currentChanged.connect((tracker, panel) => {
        window.addEventListener('message', event => {
		    if (event.data.type === 'save_notebook_content') {
			    console.log('lnb_rw_save -> lnb: save_notebook_content [id_labdoc='+event.data.id_ld+']');
                if (panel !== null){
                    let ld_content;
                    if (panel.content.model !== null){
                        ld_content = panel.content.model.toJSON();

                        window.parent.postMessage(
                          {
                            type: 'save_notebook_content', 
                            ld_content: ld_content, 
                            id_ld: event.data.id_ld
                          }, 
                        '*');
                    } else {
                        console.error('lnb_rw_save -> lnb: error while saving the notebook, content is null [id_labdoc='+event.data.id_ld+']');
                    }
                } else {
                    console.error('lnb_rw_save -> lnb: error while saving the notebook [id_labdoc='+event.data.id_ld+']');
                }
		    }
	    }, {once: false});
    });

  }
};

export default plugin;
